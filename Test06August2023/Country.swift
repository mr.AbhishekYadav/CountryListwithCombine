//
//  Country.swift
//  Test06August2023
//
//  Created by Abhishek Yadav on 06/08/23.
//

import Foundation
struct Country: Codable, Hashable {
}
struct CountriesResponse: Codable, Hashable  {
    let data: [Country]
}
class Datum: Codable {
    let iso2, iso3, country: String
    let cities: [String]
}

class Welcome: Codable {
    let error: Bool
    let msg: String
    let data: [Datum]
}
