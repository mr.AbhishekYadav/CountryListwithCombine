//
//  CountryViewModel.swift
//  Test06August2023
//
//  Created by Abhishek Yadav on 06/08/23.
//

import Foundation
import Combine

class CountryViewModel: ObservableObject {
    @Published var countries: [Country] = []
    
    func fetchCountries(completion: @escaping (Result<Welcome, Error>) -> Void) {
        let apiUrl = URL(string: "https://countriesnow.space/api/v0.1/countries")!
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: apiUrl) { (data, response, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            if let data = data {
                do {
                    let decoder = JSONDecoder()
                    let countriesResponse = try decoder.decode(Welcome.self, from: data)
                    completion(.success(countriesResponse))
                } catch {
                    completion(.failure(error))
                }
            } else {
                let error = NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey: "No data received"])
                completion(.failure(error))
            }
        }
        
        task.resume()
    }
}

