//
//  ViewController.swift
//  Test06August2023
//
//  Created by Abhishek Yadav on 06/08/23.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var activityIndicator: UIActivityIndicatorView!
    var viewModel = CountryViewModel()
    var countryNames = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }
    private func initialSetup() {
        self.tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.setupActivityIndicator()
        self.showToast(message: "Fetching countries...")
        self.apiCall()
    }
    private func setupActivityIndicator() {
        self.activityIndicator = UIActivityIndicatorView(style: .large)
        self.activityIndicator.center = view.center
        self.activityIndicator.hidesWhenStopped = true
        self.view.addSubview(activityIndicator)
    }
    private func apiCall() {
        self.showLoader()
        self.viewModel.fetchCountries { result in
            switch result {
            case .success(let welcome):
                self.apiSuccess(data: welcome.data)
            case .failure(let error):
                print("Error fetching countries: \(error)")
                self.showToast(message: "Error fetching countries")
            }
        }
    }
    private func apiSuccess(data: [Datum]?) {
        DispatchQueue.main.async {
            if let data = data {
                self.countryNames.removeAll()
                for county in data {
                    self.countryNames.append(county.country)
                }
                self.tableView.reloadData()
                self.hideLoader()
            } else {
                self.showToast(message: "No data received")
            }
        }
    }
    private func showLoader() {
        DispatchQueue.main.async {
            self.activityIndicator.startAnimating()
        }
    }
    
    private func hideLoader() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
        }
    }
    private func showToast(message: String) {
        Toast.show(message: message, controller: self)
    }
}
extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.countryNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = self.countryNames[indexPath.row]
        return cell
    }
}
